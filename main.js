var images = [{type: "triangle", src: "images/triangle.png"}, {type: "circle", src: "images/circle.png"}, {type: "square", src: "images/square.png"}];
var sounds = [{type: "triangle", src: "triangle"}, {type: "circle", src: "circle"}, {type: "square", src: "square"}];
var correct_src = "images/correct.png";
var incorrect_src = "images/incorrect.png";
var counter = 0;
var correct_answers = 0;

$(document).ready(function(){
  
  $(".begin_button").click(function(){
    $(".begin_button").addClass("hidden");
    loadStory();
  });
  
  $(".start_button").click(function(){
    $('.start_area').fadeOut(1000, function(){
      $(".start_area").addClass("hidden");
      loadQuestion();
    });
    
  });
  
  $(".correct").click(function(){
    var is_correct = $(".question_image").data("correct") == true;
    showFeedback(is_correct);
  });
  
  $(".false").click(function(){
    var is_correct = $(".question_image").data("correct") == false;
    showFeedback(is_correct);
  });
  
  $(".replay").click(function(){
    replaySound();
  });
  
  $(".play_again").click(function(){
    location.reload();
  });

});


function loadQuestion() {
  $(".feedback").addClass("hidden");
  var image = images[Math.floor(Math.random()*images.length)];
  var sound = sounds[Math.floor(Math.random()*sounds.length)];
  var correct = image.type == sound.type;
  $(".question_image").attr("src", image.src);
  $(".question_image").data("correct", correct);
  $(".question_image").data("image", image.type);
  $(".question_image").data("sound", sound.type);
  $(".question").removeClass("hidden");
  responsiveVoice.speak("The object is a " + sound.type, "UK English Female").delay(1000);
}

function replaySound(){
  type = $(".question_image").data("sound");
  responsiveVoice.speak("The object is a " + type, "UK English Female");
}

function showFeedback(correct) {
  $(".question").addClass("hidden");
  counter += 1;
  if(correct){
    $(".feedback_image").attr("src", correct_src);
    $(".feedback").removeClass("hidden");
    correct_answers += 1;
    nextAfterQuestion(counter, "Correct answer. Congratulations");
  }
  else{
    $(".feedback_image").attr("src", incorrect_src);
    $(".feedback").removeClass("hidden");
    nextAfterQuestion(counter, "Incorrect answer. Please try again.");
  }
}

function nextAfterQuestion(counter, text){
  if(counter < 5){
    speakAndReloadQuestion(text);
  }
  else {
    speakAndFinish(text);
  }
}

function speakAndReloadQuestion(text){
  responsiveVoice.speak(text, "UK English Female", {onend: loadQuestion});
}
function speakAndFinish(text){
  responsiveVoice.speak(text, "UK English Female", {onend: handleFinal});
}

function handleFinal(){
  $(".feedback").addClass("hidden");
  if(correct_answers >= 3){
    $(".final_image").attr("src", "images/happy_prince.jpg");
    responsiveVoice.speak("Congratulations. The prince is free.", "UK English Female");
  }
  else{
    $(".final_image").attr("src", "images/sad_prince.jpg");
    responsiveVoice.speak("The prince is not free. Try again.", "UK English Female");
  }
  $(".final").removeClass("hidden");
}

function loadStory(){
  
  $("#startAudio")[0].play();
  setTimeout(endStory, 61000);
}

function endStory(){
  $("#startAudio")[0].pause();
  $('.story_area').addClass("hidden");
  $('.start_area').removeClass("hidden");
}
